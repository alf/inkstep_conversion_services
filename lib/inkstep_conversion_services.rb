module ConversionServices
  if defined?(Rails)
    require 'conversion_services/engine'
  else
    require 'conversion_services/ink_step/coko/xsweet_docx_to_html_step'
  end
end
