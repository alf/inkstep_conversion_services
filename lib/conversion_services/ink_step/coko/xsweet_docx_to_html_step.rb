require 'conversion_services/version'
require 'ink_step/conversion_step'
require 'rest-client'

module InkStep::Coko
  class XsweetDocxToHtmlStep < InkStep::ConversionStep
    def perform_step
      source_file_path = find_source_file(regex: [/\.docx/])
      source_file_name = Pathname(source_file_path).sub_ext ''
      output_file_path = File.join(working_directory, "#{source_file_name}.html")

      perform_conversion(File.join(working_directory, source_file_path), output_file_path)
      success!
    end

    def version
      ConversionServices::VERSION
    end

    def required_parameters
      []
    end

    def accepted_parameters
      {}
    end

    def default_parameter_values
      {}
    end

    def self.description
      "Converts a DOCX file to HTML using XSweet"
    end

    def self.human_readable_name
      "XSweet DOCX-to-HTML"
    end

    def perform_conversion(source, destination)
      log_as_step "Posting #{source} to http://xsweet:80/"

      response = RestClient.post 'http://xsweet:80/', :input => File.new(source, 'rb')

      log_as_step "Received response #{response.code}"

      File.write(destination, response.body)
    end
  end
end
