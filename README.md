# InkStep - ConversionServices

This gem runs as a plugin for [the INK API](https://gitlab.coko.foundation/INK/ink-api), providing steps that use web services to perform file conversion.

### DOCXToHTMLStep
This step takes a DOCX file as input and outputs HTML.

## Installation
 
See [the INK documentation](https://gitlab.coko.foundation/INK/ink-api) for detailed instructions for installing this plugin into an instance of INK.
